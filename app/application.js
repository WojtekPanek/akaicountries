/**
 * Created by Wojciech Panek on 2015-06-11.
 */


var application = angular.module('countriesApp', [
    'ui.router'
]);

application.config(['$locationProvider', '$stateProvider', '$urlRouterProvider',
    function($locationProvider, $stateProvider, $urlRouterProvider){

    $locationProvider.hashPrefix('');

    $stateProvider.state('countryList', {
        url: "/",
        templateUrl: "app/view/country-list.html"
    });

    $stateProvider.state('countryView', {
        url: "/:name",
        templateUrl: "app/view/country-view.html"
    });

    $urlRouterProvider.otherwise('/');
}]);






