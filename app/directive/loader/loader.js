/**
 * Created by Wojciech Panek on 2015-06-11.
 */
application.directive('loader', [
    function(){
        return {
            restrict: 'AE',
            replace: true,
            transclude: true,
            templateUrl: 'app/directive/loader/loader.html',
            scope: {
                isLoading: '='
            },
            link: function($scope, element) {
                var scale = 10;
                var negative = true;

                $scope.animation = setInterval(function(){
                    angular.element(element).find(".loader").css('transform', 'scale(' + scale/10 + ')');

                    if(negative) scale -= 1; else scale += 1;
                    if(scale == 0 || scale == 10) negative = !negative;

                }, 50);

                $scope.$on('$destroy', function() {
                    clearInterval($scope.animation);
                });
            }
        }
    }
]);