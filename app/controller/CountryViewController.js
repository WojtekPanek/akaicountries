/**
 * Created by Wojciech Panek on 2015-06-11.
 */
application.controller('countryViewController', ['$scope', '$stateParams', 'countryService',
    function($scope, $stateParams, countryService){

        $scope.country = null;
        $scope.loadingCountry = true;
        $scope.countryNotFound = false;

        countryService.get($stateParams.name).then(function(country){
            $scope.country = country;
            $scope.loadingCountry = false;
        }, function(){
            $scope.countryNotFound = true;
        });
}]);