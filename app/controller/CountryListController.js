/**
 * Created by Wojciech Panek on 2015-06-11.
 */
application.controller('countryListController', ['$scope', 'countryService',
    function($scope, countryService){
        $scope.countries = [];
        $scope.loadingCountries = true;

        $scope.countryModel = {
            name: "",
            symbol: ""
        };

        countryService.getList().then(function(list){
            $scope.countries = list;
            $scope.loadingCountries = false;
        });

        $scope.addCountry = function() {
            $scope.countries.push($scope.countryModel);
            $scope.countryModel = {
                name: "",
                symbol: ""
            }
        }

}]);