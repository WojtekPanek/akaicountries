/**
 * Created by Wojciech Panek on 2015-06-11.
 */
application.factory('countryFactory', ['$http', '$q',
    function ($http, $q) {
        return  {
            getList: function() {
                var deferred = $q.defer();

                $http.get('https://restcountries.eu/rest/v1/all').success(function (data) {
                    var result = [];
                    for (var i = 0; i < data.length; i++) {
                        result.push({
                            name: data[i].name,
                            symbol: data[i].alpha2Code
                        });
                    }
                    deferred.resolve(result);
                });

                return deferred.promise;
            },

            get: function (name) {
                var deferred = $q.defer();

                $http.get('https://restcountries.eu/rest/v1/name/' + name).success(function (data) {
                    if(data.length){
                        deferred.resolve(data[0]);
                    } else {
                        deferred.reject();
                    }
                });

                return deferred.promise;
            }
        };
}]);